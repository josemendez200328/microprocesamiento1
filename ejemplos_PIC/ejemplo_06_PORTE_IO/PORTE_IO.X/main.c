#include <xc.h>

#pragma config FOSC = HS 
#pragma config WDTE = OFF 
#pragma config PWRTE = OFF 
#pragma config MCLRE = ON
#pragma config CP = OFF 
#pragma config CPD = OFF 
#pragma config BOREN = OFF 
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF 
#pragma config DEBUG = OFF 

#define _XTAL_FREQ 20000000


int main(void)
{  
    ANSEL = 0x00;
    PORTE = 0x00;
    TRISE = 0x01;
    
    while(1)
    {
        if(RE0 == 0)
        {
            __delay_ms(100);

            if(RE0 == 0)
            {
                if(PORTE < 0x06)
                {
                    PORTE += 0x02;
                    __delay_ms(1000);
                }
                else
                {
                    PORTE = 0x00;
                    __delay_ms(1000);
                }
            }
        }
    }       

    return 0;
}