#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <xc.h>
#include "lcd.h"

#pragma config FOSC = HS 
#pragma config WDTE = OFF 
#pragma config PWRTE = OFF 
#pragma config MCLRE = ON
#pragma config CP = OFF 
#pragma config CPD = OFF 
#pragma config BOREN = OFF 
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF 
#pragma config DEBUG = OFF 

#define _XTAL_FREQ 20000000

unsigned char i = 0;
unsigned char c = 0;
char text[10] = " Micros 1 ";

int main ( void )
{    
    LCD lcd = { &PORTC, 2, 3, 4, 5, 6, 7 }; // PORT, RS, EN, D4, D5, D6, D7

    LCD_Init(lcd);

    while(1)
    {
        LCD_Clear();
        
        LCD_Set_Cursor(0,0);
        LCD_putrs("  Hola mundo!  ");

		LCD_Set_Cursor(1,3);
		for (c=0; c <10; c++)
        { 
			 LCD_putc(text[c]);
			 for (i = 0; i < 30; ++i )
             { 
			 	__delay_ms(10);
			 }
		}
		
		for (i = 0; i < 100; i++)
        {
		 	__delay_ms(10);
		}
	}

    return 0;
}