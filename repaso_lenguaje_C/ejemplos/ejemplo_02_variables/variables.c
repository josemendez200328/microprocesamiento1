#include<stdio.h>


int main(void)
{
    int i = 0, j = 0;

    float decimal = -1.9423;

    char letra = 'a';
    char ciudad[] = "Santa Marta";
    char departamento[] = {'M', 'a', 'g', 'd', 'a', 'l', 'e', 'n', 'a'};

    int I[3][3] = {{1, 0, 0},
                   {0, 1, 0},
                   {0, 0, 1}};


    printf("Matriz identidad:\n");

    for(i=0; i<3; i++)
    {
        for(j=0; j<3; j++)
        {
            printf("%d\t",I[i][j]);
         }

        printf("\n");

    }

    printf("\n%f: es un número decimal.\n", decimal);

    printf("\n%c: es una letra.\n", letra);

    printf("\nLa ciudad es: %s.\n", ciudad);

    printf("\nEl departamento es:\n");


    for(i=0; i<9; i++)
    {
        printf("%c", departamento[i]);
    }

    printf("\n");


    return 0;
}
