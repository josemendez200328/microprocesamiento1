# El problema de un granjero, un lobo, una cabra y un repollo.
_Propuesto por el usuario Wontonimo_

:walking: :wolf: :goat: :leaves:

<br>

## Objetivo:
Un granjero necesita pasar el río con un lobo, una cabra y un repollo.
Para pasar el río el granjero tiene las siguientes restricciones:

- El granjero puede moverse libremente de una orilla del río a otra y puede llevar con él alguna otra entidad _(lobo, cabra o repollo)_. 
- El lobo no puede estar en el mismo lugar en el que está la cabra sin la presencia del granjero.
    - <<Si el granjero no está presente, el lobo se comerá a la cabra>>
    - :wolf: :no_entry_sign: :goat:
- La cabra no puede estar en el mismo lugar en el está el repollo sin la presencia del granjero.
    - <<Si el granjero no está presente, la cabra se comerá al repollo>>
    - :goat: :no_entry_sign: :leaves:

<br>

El río tiene dos orillas: **L** _(orilla izquierda)_ y **R** _(orilla derecha)_.

Las posiciones iniciales y las posiciones objetivo deben seguir el siguiente orden:

<br>

| Granjero | lobo | cabra | repollo |
|:--:|:--:|:--:|:--:|

<br>

>### Importante:
>**L L L R** significa que el granjero, el lobo y la cabra están en la orilla izquierda, mientras que el repollo está en la orilla derecha.

<br>

Sin romper las restricciones descritas anteriormente, imprima en la terminal los estados legales mínimos que comienzan en el estado inicial para llegar al estado objetivo _(En el mensaje en la terminal debe incluir el estado inicial y objetivo)_.

<br>

## Ejemplo

| Estado | Representación |
|:--:|:--:|
| inicial | L L L R |
| objetivo | L L L L |

### Explicación:
- **L L L R** : Estado inicial.
- **R L R R** : El grajero lleva a la cabra a la orilla derecha.
- **L L R L** : El granjero lleva al repollo a la orilla izquierda.
- **R L R L** : El granjero regresa a la orilla derecha con la cabra.
- **L L L L** : El granjero lleva a la cabra a la orilla izquierda.

<br>

>#### Restricciones:
>Todos los problemas tienen una solución. Y cada solución tiene menos de 20 estados de transición. El granjero sólo puede llevar consigo una entidad a la vez.

<br>

### Vídeo de apoyo
[Los Simpson: acertijo del zorro, el pato y el maíz](https://www.youtube.com/watch?v=Lp-nuB0k5Sg)

<br>

## Problemas:
### Problema 01:
| Estado | Representación |
|:--:|:--:|
| inicial | L L L L |
| objetivo | R L R L |

#### Resultado esperado:
```bash
L L L L
R L R L
```

<br>

### Problema 02:
| Estado | Representación |
|:--:|:--:|
| inicial | L L L R |
| objetivo | L L L R |

#### Resultado esperado:
```bash
L L L R
```

<br>

### Problema 03:
| Estado | Representación |
|:--:|:--:|
| inicial | L L L L |
| objetivo | R R R R |

#### Resultado esperado:
```bash
L L L L
R L R L
L L R L
R L R R
L L L R
R R L R
L R L R
R R R R
```

<br>

### Problema 04:
| Estado | Representación |
|:--:|:--:|
| inicial | R L R L |
| objetivo | L R L R |

#### Resultado esperado:
```bash
R L R L
L L R L
R L R R
L L L R
R R L R
L R L R
```


<br>

## Reto

Desarrolle un programa en lenguaje C que resuelva el problema expuesto, escriba el método empleado para realizar la solución en un artículo científico [Descargar aquí el template del artículo](https://gitlab.com/diegorestrepo/microprocesamiento1/-/raw/master/Documentos/Access-Template.doc?inline=false).

### Preguntas orientadoras para resolver el problema:
- ¿Qué es un grafo?
- ¿Cómo se puede representar un grafo dentro de un código?
- ¿Cuales son los algoritmos de búsqueda en los grafos?

### Sugerencias para resolver el problema:
- Represente el problema en un grafo.
- Implemente un algoritmo de búsqueda, inicie por el algoritmo Breadth-first search (BFS).
