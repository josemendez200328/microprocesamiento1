\babel@toc {spanish}{}\relax 
\beamer@sectionintoc {1}{Bienvenida}{3}{0}{1}
\beamer@sectionintoc {2}{Herramientas de desarrollo}{5}{0}{2}
\beamer@sectionintoc {3}{Generalidades}{7}{0}{3}
\beamer@sectionintoc {4}{Variables}{17}{0}{4}
\beamer@sectionintoc {5}{Estructuras de control}{21}{0}{5}
\beamer@sectionintoc {6}{Funciones}{33}{0}{6}
\beamer@sectionintoc {7}{Librerías}{35}{0}{7}
\beamer@sectionintoc {8}{Ejercicios}{39}{0}{8}
