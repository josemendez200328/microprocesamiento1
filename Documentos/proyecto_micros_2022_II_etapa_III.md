# Mikro mouse

Desarrolle un prototipo de _"carrito"_  el cual llege desde el punto *A* hasta el punto *B* de un laberinto por el camino más optimo.

<br>

## Requisitos:
1. Utilice el prototipo desarrollado en la etapa II.
2. Implemente algún algoritmo de búsqeda en grafos.
3. Los códigos y el diseño deben estar almacenados en un repositorio [gitlab](https://about.gitlab.com) o [github](https://github.com)
