# Mario kart

Desarrolle un prototipo de _"carrito"_  al cuál se le añadiran tres globos en la parte posterior y una _"lanza"_ al frente. El objetivo es romper los globos del contendiente, el judador que al final quede con más globos es el ganador del encuentro.

<br>

## Requisitos:
1. El control debe ser alambrico y activado por medio de las resistencias de pull up, debe desarrollar un mecanisco que evite obstaculizar el paso de otro competidor. El prototipo debe poder moverse en todas las dimensiones de una superficie.
2. La lanza no debe tener una longitud mayor a 6 cm.
3. Diseño del carrito [consultar aquí](https://www.thingiverse.com/thing:2583461).
4. El circuito debe ser presentado en PCB.
5. Los códigos y el diseño deben estar almacenados en un repositorio [gitlab](https://about.gitlab.com) o [github](https://github.com)
