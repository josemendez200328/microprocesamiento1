# Exploración de un laberinto

Desarrolle un prototipo de _"carrito"_  que explore y memorice un laberinto, para luego mostrar la representación del mismo en una LCD.

<br>

## Requisitos:
1. Utilice el prototipo de _"carrito"_ desarrollado en la etapa I.
2. Determine los sensores más adecuados para cumplir con este reto y la estrategía que debe seguir el _"carrito"_.
3. Los códigos y el diseño deben estar almacenados en un repositorio [gitlab](https://about.gitlab.com) o [github](https://github.com)
